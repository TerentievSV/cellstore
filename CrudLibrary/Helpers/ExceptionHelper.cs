﻿using System;
using System.Text;

namespace CrudLibrary.Helpers
{
    public static class ExceptionHelper
    {
        public static string GetFullExceptionMessage(this Exception exception)
        {
            var message = new StringBuilder();
            message.AppendFormat("{0}\n", exception.Message);

            var tmpInnerException = exception.InnerException;
            while (tmpInnerException != null)
            {
                message.AppendFormat("{0}\n", tmpInnerException);
                tmpInnerException = tmpInnerException.InnerException;
            }

            return message.ToString();
        }
    }
}
