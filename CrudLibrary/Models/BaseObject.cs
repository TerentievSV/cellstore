﻿namespace CrudLibrary.Models
{
    public class BaseObject
    {
        /// <summary>
        /// Id объекта
        /// </summary>
        public int Id { get; set; }
    }
}
