﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrudLibrary.Helpers;
using CrudLibrary.Infrastructure;
using CrudLibrary.Models;
using Microsoft.EntityFrameworkCore;

namespace CrudLibrary
{
    public abstract class GenericRepository<T> : IGenericRepository<T>
        where T : BaseObject
    {
        protected readonly DbContext DbContext;
        private readonly DbSet<T> _dbSet;

        protected GenericRepository(DbContext dbContext)
        {
            DbContext = dbContext;
            _dbSet = dbContext.Set<T>();
        }

        public async Task<int> CreateAsync(T entity)
        {
            int rowsCount;

            using (var transaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    _dbSet.Add(entity);
                    rowsCount = await DbContext.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();

                    var errorBaseMessage = exception.GetFullExceptionMessage();
                    throw new Exception(errorBaseMessage);
                }
            }

            return rowsCount;
        }

        public async Task<IEnumerable<T>> ReadEntitiesAsync()
        {
            var objects = await _dbSet.ToListAsync();
            return objects;
        }

        /// <summary>
        /// Возвращает объект по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> ReadAsync(int id)
        {
            var entity = await _dbSet
                .SingleOrDefaultAsync(e => e.Id == id);
            return entity;
        }

        /// <summary>
        /// Обновление объекта
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<int> UpdateAsync(T entity)
        {
            int rowsCnt;

            using (var transaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    DbContext.Update(entity);
                    rowsCnt = await DbContext.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();

                    var errorBaseMessage = exception.GetFullExceptionMessage();
                    throw new Exception(errorBaseMessage);
                }
            }

            return rowsCnt;
        }

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract Task<int> DeleteAsync(int id);

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected async Task<int> DeleteAsync(T entity)
        {
            int rowsCnt;

            using (var transaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    DbContext.Entry(entity).State = EntityState.Deleted;
                    rowsCnt = await DbContext.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();

                    var errorBaseMessage = exception.GetFullExceptionMessage();
                    throw new Exception(errorBaseMessage);
                }
            }

            return rowsCnt;
        }
    }
}
