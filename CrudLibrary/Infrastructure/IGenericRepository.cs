﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrudLibrary.Models;

namespace CrudLibrary.Infrastructure
{
    public interface IGenericRepository<T> where T : BaseObject
    {
        /// <summary>
        /// Создание объекта
        /// </summary>
        /// <param name="entity">Созданный в памяти объект</param>
        Task<int> CreateAsync(T entity);

        /// <summary>
        /// Возвращает список табличных объектов
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> ReadEntitiesAsync();

        /// <summary>
        /// Возвращает объект по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> ReadAsync(int id);

        /// <summary>
        /// Обновление объекта
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(T entity);

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(int id);
    }
}
