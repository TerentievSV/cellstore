﻿using System.Threading.Tasks;
using CellStore.Infrastructure;
using CellStore.Models;
using Microsoft.AspNetCore.Mvc;

namespace CellStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPhoneRepository _phoneRepository;
        
        public HomeController(IPhoneRepository phoneRepository)
        {
            _phoneRepository = phoneRepository;
        }

        public async Task<IActionResult> Index()
        {
            var phones = await _phoneRepository.ReadEntitiesAsync();
            return View(phones);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Phone phone)
        {
            await _phoneRepository.CreateAsync(phone);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id != null)
            {
                var phone = await _phoneRepository.ReadAsync(id.Value);
                if (phone != null)
                    return View(phone);
            }

            return NotFound();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                var phone = await _phoneRepository.ReadAsync(id.Value);
                if (phone != null)
                    return View(phone);
            }

            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Phone phone)
        {
            await _phoneRepository.UpdateAsync(phone);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Phone phone = await _phoneRepository.ReadAsync(id.Value);
                if (phone != null)
                    return View(phone);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id != null)
            {
                await _phoneRepository.DeleteAsync(id.Value);
                return RedirectToAction("Index");
            }
            return NotFound();
        }
    }
}
