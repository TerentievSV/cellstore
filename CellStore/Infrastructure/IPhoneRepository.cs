﻿using CellStore.Models;
using CrudLibrary.Infrastructure;

namespace CellStore.Infrastructure
{
    public interface IPhoneRepository : IGenericRepository<Phone>
    {
    }
}
