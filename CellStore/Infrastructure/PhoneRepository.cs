﻿using System;
using System.Threading.Tasks;
using CellStore.Models;
using CrudLibrary;
using CrudLibrary.Helpers;

namespace CellStore.Infrastructure
{
    public class PhoneRepository : GenericRepository<Phone>, IPhoneRepository
    {
        public PhoneRepository(CellContext dbContext) : base(dbContext)
        {

        }

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<int> DeleteAsync(int id)
        {
            int rowsCnt;
            
            try
            {
                var phone = new Phone { Id = id };
                rowsCnt = await DeleteAsync(phone);
            }
            catch (Exception exception)
            {
                var errorBaseMessage = exception.GetFullExceptionMessage();
                throw new Exception(errorBaseMessage);
            }

            return rowsCnt;
        }
    }
}
