﻿using CrudLibrary.Models;

namespace CellStore.Models
{
    public class Phone : BaseObject
    {
        public string Name { get; set; } // название смартфона
        public string Company { get; set; } // компания
        public int Price { get; set; } // цена
    }
}
