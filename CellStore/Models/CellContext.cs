﻿using Microsoft.EntityFrameworkCore;

namespace CellStore.Models
{
    public class CellContext : DbContext
    {
        public CellContext(DbContextOptions<CellContext> options) 
            : base(options)
        {
            
        }

        public DbSet<Phone> Phones { get; set; }
    }
}
